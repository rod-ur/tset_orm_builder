<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tb10 extends Model
{
    public function tb5()
    {
        return $this->hasOne('App\tb5');
    }

    public function tb9()
    {
        return $this->hasOne('App\tb9');
    }
}
