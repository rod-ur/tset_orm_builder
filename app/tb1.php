<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tb1 extends Model
{
    public function cat1()
    {
        return $this->hasOne('App\cat1');
    }

    public function cat2()
    {
        return $this->hasOne('App\cat2');
    }
}
