<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tb8 extends Model
{
    public function tb7()
    {
        return $this->hasOne('App\tb7', 'id', 'tb7s_id');
    }

    public function cat4()
    {
        return $this->hasOne('App\cat4', 'id', 'cat4_id');
    }
}
