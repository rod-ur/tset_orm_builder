<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tb5 extends Model
{
    public function tb2()
    {
        return $this->hasOne('App\tb2');
    }

    public function tb5()
    {
        return $this->hasOne('App\tb5');
    }

    public function tb10()
    {
        return $this->belongsToMany('App\tb10', 'tb5s', 'id', 'id');
    }
}
