<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tb2 extends Model
{
    public function tb1()
    {
        return $this->hasOne('App\tb1');
    }

    public function cat3()
    {
        return $this->hasOne('App\cat3');
    }

    public function tb5()
    {
        return $this->belongsToMany('App\tb5', 'tb2s', 'id', 'id');
    }
}
