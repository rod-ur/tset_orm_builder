<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tb4 extends Model
{
    public function tb1()
    {
        return $this->hasOne('App\tb1');
    }

    public function tb3()
    {
        return $this->hasOne('App\tb3');
    }
}
