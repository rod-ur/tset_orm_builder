<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tb3 extends Model
{
    public function cat3()
    {
        return $this->hasOne('App\cat3', 'id', 'cat3_id');
    }

    public function cat5()
    {
        return $this->hasOne('App\cat5', 'id', 'cat5_id');
    }
}
