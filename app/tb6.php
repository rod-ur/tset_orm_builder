<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tb6 extends Model
{
    public function tb5()
    {
        return $this->hasOne('App\tb5');
    }

    public function tb1()
    {
        return $this->hasOne('App\tb1');
    }
}
