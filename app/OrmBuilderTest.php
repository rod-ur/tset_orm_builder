<?php

namespace App;

use App\stats as Stat;
use Illuminate\Support\Facades\DB;

class OrmBuilderTest
{
    public static $c1Max = 10;
    public static $c2Max = 10;
    public static $c3Max = 10;
    public static $c4Max = 10;
    public static $c5Max = 10;

    public static $t1Max = 10;
    public static $t2Max = 10;
    public static $t3Max = 10;
    public static $t4Max = 10;
    public static $t5Max = 10;
    public static $t6Max = 10;
    public static $t7Max = 10;
    public static $t8Max = 10;
    public static $t9Max = 10;
    public static $t10Max = 10;

    /**
     * A basic functional test example.
     *
     * @return void
     */
    private function insertStat(string $desc, \Closure $f, $type = null)
    {
        $s = new Stat;
        $s->desc = $desc;
        $s->start = microtime(true);
        $f();
        $s->end = microtime(true);
        $s->elapsed = $s->end - $s->start;
        if ($type) $s->type = $type;
        $s->save();
    }

    private function seedStat($class, $cuantos = 1)
    {
        $chunk = 15000;
        for ($i = 0; $i < $cuantos; $i += $chunk) {
            $restantes = $cuantos - $i;
            $inserta = $chunk > $restantes ? $restantes : $chunk;
            $desc = "Insertando $inserta en $class; $i/$cuantos restan $restantes";
            print "$desc\n";
            $t = time();
            $this->insertStat($desc,
                function () use ($class, $inserta) {
                    factory($class, $inserta)->create();
                });
        }
    }

    public function seed()
    {
        $this->seedStat(cat1::class, rand(250, 6500));
        self::$c1Max = cat1::max('id');

        $this->seedStat(cat2::class, rand(250, 6500));
        self::$c2Max = cat2::max('id');

        $this->seedStat(cat3::class, rand(250, 6500));
        self::$c3Max = cat3::max('id');

        $this->seedStat(cat4::class, rand(250, 6500));
        self::$c4Max = cat4::max('id');

        $this->seedStat(cat5::class, rand(250, 6500));
        self::$c5Max = cat5::max('id');

        $this->seedStat(tb9::class, rand(10000, 215000));
        self::$t9Max = tb9::max('id');

        $this->seedStat(tb1::class, rand(10000, 215000));
        self::$t1Max = tb1::max('id');

        $this->seedStat(tb2::class, rand(10000, 215000));
        self::$t2Max = tb2::max('id');

        $this->seedStat(tb3::class, rand(10000, 215000));
        self::$t3Max = tb3::max('id');

        $this->seedStat(tb4::class, rand(10000, 215000));
        self::$t4Max = tb4::max('id');

        $this->seedStat(tb5::class, rand(10000, 215000));
        self::$t5Max = tb5::max('id');

        $this->seedStat(tb6::class, rand(10000, 215000));
        self::$t6Max = tb6::max('id');

        $this->seedStat(tb7::class, rand(10000, 215000));
        self::$t7Max = tb7::max('id');

        $this->seedStat(tb8::class, rand(10000, 215000));
        self::$t8Max = tb8::max('id');

        $this->seedStat(tb10::class, rand(10000, 215000));
        self::$t10Max = tb10::max('id');
    }


    public function test()
    {
        $maxQuieries = 250;
        $nBuilder = 0;
        $nOrm = 0;

        $step = 0;
        do {
            $step++;
            $bFinish = $nBuilder >= $maxQuieries;
            $oFinish = $nOrm >= $maxQuieries;
            $r = rand(0, 1);
            if ($r % 2) {
                if ($oFinish) continue;
                $nOrm++;
                $desc = "OrmQuery #$nOrm";
                $this->insertStat($desc,
                    function () {
                        $this->ormQuery();
                    }, 2);
                print "Haciendo query #$nOrm orm\n";
            } else {
                if ($bFinish) continue;
                $nBuilder++;
                $desc = "BuilderQuey #$nBuilder";
                $this->insertStat($desc,
                    function () {
                        $this->builderQuery();
                    }, 2);
                print "Haciendo query #$nBuilder builder\n";
            }
        } while (!($oFinish && $bFinish));

        print "Pasos: $step\n";
    }

    public function ormQuery()
    {
        DB::enableQueryLog(); // Enable query log

        $res = tb8::with('tb7.tb3.cat3.tb2.tb5.tb10', 'cat4')
            ->where('id', '<', 50)
//            ->groupBy('id')
//            ->first();
            ->get();
        $sum = 0;
        $nombres = [];
        foreach ($res as $r) {
            foreach ($r->tb7->tb3->cat3->tb2 as $tb2) {
                foreach ($tb2->tb5 as $tb5) {
                    foreach ($tb5 as $tb10) {
                        dd($tb5->tb10);
                        $nombres[] = $tb5->tb10->nombre;
                        $sum++;
                    }
                }
            }
        }

        return [$sum . $nombres];
    }

    public function builderQuery()
    {
        $res = DB::table('tb8s as t8s')
            ->select(DB::raw('count(*) as cuantos, t8s.*'))
            ->leftJoin('cat4s as c4s', 't8s.cat4_id', '=', 'c4s.id')
            ->leftJoin('tb7s as t7s', 't8s.tb7s_id', '=', 't7s.id')
            ->leftJoin('tb3s as t3s', 't7s.tb3s_id', '=', 't3s.id')
            ->leftJoin('cat3s as c3s', 't3s.cat3_id', '=', 'c3s.id')
            ->leftJoin('tb2s as t2s', 'c3s.id', '=', 't2s.cat3_id')
            ->join('tb5s as t5s', 't2s.id', '=', 't5s.tb2s_id')
            ->join('tb10s as t10s', 't5s.id', '=', 't10s.tb5s_id')
            ->where('t8s.id', '<', 50)
            ->groupBy('t8s.id')
            ->get();
    }
}
