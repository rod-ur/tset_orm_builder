<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tb7 extends Model
{
    public function tb6()
    {
        return $this->hasOne('App\tb6', 'id', 'tb6s_id');
    }

    public function tb3()
    {
        return $this->hasOne('App\tb3', 'id', 'tb3s_id');
    }
}
