<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTb7sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb7s', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->mediumText('descripcion');
            $table->boolean('activo')->default(true);
            $table->timestamps();

            $table->integer('tb6s_id')->unsigned();
            $table->foreign('tb6s_id')->references('id')->on('tb6s');

            $table->integer('tb3s_id')->unsigned();
            $table->foreign('tb3s_id')->references('id')->on('tb3s');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tb7s');
    }
}
