<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTb6sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb6s', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->mediumText('descripcion');
            $table->boolean('activo')->default(true);
            $table->timestamps();

            $table->integer('tb5s_id')->unsigned();
            $table->foreign('tb5s_id')->references('id')->on('tb5s');

            $table->integer('tb1s_id')->unsigned();
            $table->foreign('tb1s_id')->references('id')->on('tb1s');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tb6s');
    }
}
