<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTb1sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb1s', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->mediumText('descripcion');
            $table->boolean('activo')->default(true);
            $table->timestamps();

            $table->integer('cat1_id')->unsigned();
            $table->foreign('cat1_id')->references('id')->on('cat1s');

            $table->integer('cat2_id')->unsigned();
            $table->foreign('cat2_id')->references('id')->on('cat2s');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tb1s');
    }
}
