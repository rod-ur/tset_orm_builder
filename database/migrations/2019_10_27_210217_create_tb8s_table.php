<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTb8sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb8s', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->mediumText('descripcion');
            $table->boolean('activo')->default(true);
            $table->timestamps();

            $table->integer('tb7s_id')->unsigned();
            $table->foreign('tb7s_id')->references('id')->on('tb7s');

            $table->integer('cat4_id')->unsigned();
            $table->foreign('cat4_id')->references('id')->on('cat4s');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tb8s');
    }
}
