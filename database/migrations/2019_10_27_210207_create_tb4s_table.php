<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTb4sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb4s', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->mediumText('descripcion');
            $table->boolean('activo')->default(true);
            $table->timestamps();

            $table->integer('tb1s_id')->unsigned();
            $table->foreign('tb1s_id')->references('id')->on('tb1s');

            $table->integer('tb3s_id')->unsigned();
            $table->foreign('tb3s_id')->references('id')->on('tb3s');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tb4s');
    }
}
