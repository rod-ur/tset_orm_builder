<?php

use App\cat1;
use App\cat2;
use App\cat3;
use App\cat4;
use App\cat5;
use App\tb1;
use App\tb2;
use App\tb3;
use App\tb4;
use App\tb5;
use App\tb6;
use App\tb7;
use App\tb8;
use App\tb9;
use App\tb10;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(cat1::class, function ($faker) {
    return [
        'nombre' => $faker->name,
        'descripcion' => $faker->realText(rand(30, 180)),
    ];
});

$factory->define(cat2::class, function ($faker) {
    return [
        'nombre' => $faker->name,
        'descripcion' => $faker->realText(rand(30, 180)),
    ];
});

$factory->define(cat3::class, function ($faker) {
    return [
        'nombre' => $faker->name,
        'descripcion' => $faker->realText(rand(30, 180)),
    ];
});

$factory->define(cat4::class, function ($faker) {
    return [
        'nombre' => $faker->name,
        'descripcion' => $faker->realText(rand(30, 180)),
    ];
});

$factory->define(cat5::class, function ($faker) {
    return [
        'nombre' => $faker->name,
        'descripcion' => $faker->realText(rand(30, 180)),
    ];
});

$factory->define(tb1::class, function ($faker) {
    $r1 = rand(1, \App\OrmBuilderTest::$c1Max);
    $r2 = rand(1, \App\OrmBuilderTest::$c2Max);

    return [
        'nombre' => $faker->name,
        'descripcion' => $faker->realText(rand(30, 180)),
        'cat1_id' => $r1,
        'cat2_id' => $r2,
    ];
});

$factory->define(tb2::class, function ($faker) {
    $r1 = rand(1, \App\OrmBuilderTest::$t1Max);
    $r2 = rand(1, \App\OrmBuilderTest::$c3Max);

    return [
        'nombre' => $faker->name,
        'descripcion' => $faker->realText(rand(30, 180)),
        'tb1s_id' => $r1,
        'cat3_id' => $r2,
    ];
});

$factory->define(tb3::class, function ($faker) {
    $r1 = rand(1, \App\OrmBuilderTest::$c3Max);
    $r2 = rand(1, \App\OrmBuilderTest::$c5Max);

    return [
        'nombre' => $faker->name,
        'descripcion' => $faker->realText(rand(30, 180)),
        'cat3_id' => $r1,
        'cat5_id' => $r2,
    ];
});

$factory->define(tb4::class, function ($faker) {
    $r1 = rand(1, \App\OrmBuilderTest::$t1Max);
    $r2 = rand(1, \App\OrmBuilderTest::$t3Max);

    return [
        'nombre' => $faker->name,
        'descripcion' => $faker->realText(rand(30, 180)),
        'tb1s_id' => $r1,
        'tb3s_id' => $r2,
    ];
});

$factory->define(tb5::class, function ($faker) {
    $r1 = rand(1, \App\OrmBuilderTest::$t2Max);
    $r2 = rand(1, \App\OrmBuilderTest::$c5Max);

    return [
        'nombre' => $faker->name,
        'descripcion' => $faker->realText(rand(30, 180)),
        'tb2s_id' => $r1,
        'cat5_id' => $r2,
    ];
});

$factory->define(tb6::class, function ($faker) {
    $r1 = rand(1, \App\OrmBuilderTest::$t1Max);
    $r2 = rand(1, \App\OrmBuilderTest::$t5Max);

    return [
        'nombre' => $faker->name,
        'descripcion' => $faker->realText(rand(30, 180)),
        'tb1s_id' => $r1,
        'tb5s_id' => $r2,
    ];
});

$factory->define(tb7::class, function ($faker) {
    $r1 = rand(1, \App\OrmBuilderTest::$t6Max);
    $r2 = rand(1, \App\OrmBuilderTest::$t3Max);

    return [
        'nombre' => $faker->name,
        'descripcion' => $faker->realText(rand(30, 180)),
        'tb6s_id' => $r1,
        'tb3s_id' => $r2,
    ];
});

$factory->define(tb8::class, function ($faker) {
    $r1 = rand(1, \App\OrmBuilderTest::$t7Max);
    $r2 = rand(1, \App\OrmBuilderTest::$c4Max);

    return [
        'nombre' => $faker->name,
        'descripcion' => $faker->realText(rand(30, 180)),
        'tb7s_id' => $r1,
        'cat4_id' => $r2,
    ];
});

$factory->define(tb9::class, function ($faker) {
    return [
        'nombre' => $faker->name,
        'descripcion' => $faker->realText(rand(30, 180)),
    ];
});

$factory->define(tb10::class, function ($faker) {
    $r1 = rand(1, \App\OrmBuilderTest::$t5Max);
    $r2 = rand(1, \App\OrmBuilderTest::$t9Max);

    return [
        'nombre' => $faker->name,
        'descripcion' => $faker->realText(rand(30, 180)),
        'tb5s_id' => $r1,
        'tb9s_id' => $r2,
    ];
});
